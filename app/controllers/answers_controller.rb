class AnswersController < ApplicationController

def create
  @question = Question.find(params[:question_id])
  @answer = @question.answer.build(params[:id])
  @answer.user = current_user
  @answer.save
  redirect_to questuins_url
end

def destroy
  @question = Question.find(params[:question_id])
  @answer = @question.answer.find(params[:id]).destroy
  redirect_to questuins_url
end

private
def answers_params
  params.require(:answer).permit(:content)
end

end
